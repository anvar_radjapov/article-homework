package storage

import "bootcamp/article/models"

type StorageI interface {
	Article() ArticleRepoI
	Author() AuthorRepoI
}

type ArticleRepoI interface {
	Create(entity models.ArticleCreateModel) (err error)
	GetList(query models.Query) (resp []models.ArticleListItem, err error)
	GetByID(ID int) (resp models.Article, err error)
	Update(entity models.ArticleUpdateModel) (err error)
	Delete(ID int) (err error)
}

type AuthorRepoI interface {
	Create(entity models.Person) (err error)
	GetList(query models.Query) (resp []models.PersonListModel, err error)
	GetByID(ID int) (resp models.PersonListModel, err error)
	Update(entity models.PersonUpdateModel) (err error)
	Delete(ID int) (err error)
}
