package handlers

import (
	"bootcamp/article/models"
	// "fmt"
	"strconv"

	"github.com/gin-gonic/gin"
)

// CreateHandlerAuthor godoc
// @Router /authors [POST]
// @Summary Create an Author Profile
// @Description it creates an author profile from json body
// @ID create-handeler-author
// @Tags CRUD Author
// @Accept  json
// @Produce  json
// @Param data body models.Person true "Author data"
// @Success 200  {object} models.SuccessResponse
// @Failure 400 {object} models.ErrorResponse
// @Failure default {object} models.DefaultError
func (h *Handler) CreateHandlerAuthor(c *gin.Context) {
	var author models.Person
	err := c.BindJSON(&author)
	if err != nil {
		c.JSON(400, models.ErrorResponse{
			Code:    400,
			Message: err.Error(),
		})
		return
	}

	err = h.strg.Author().Create(author)
	if err != nil {
		c.JSON(400, models.DefaultError{
			Message: err.Error(),
		})
		return
	}

	c.JSON(200, models.SuccessResponse{
		Message: "Ok",
	})
}

// GetAuthorList godoc
// @Router /authors [GET]
// @ID get-author-list
// @Summary List authors
// @Description get all author profiles
// @Param offset query int false "offset"
// @Param limit query int false "limit"
// @Param search query string false "search string"
// @Tags CRUD Author
// @Accept  json
// @Produce  json
// @Success 200 {array} models.PersonListModel
// @Failure default {object} models.DefaultError
func (h *Handler) GetAuthorList(c *gin.Context) {
	offset, err := h.getOffsetParam(c)
	if err != nil {
		c.JSON(400, models.DefaultError{
			Message: err.Error(),
		})
	}

	limit, err := h.getLimitParam(c)
	if err != nil {
		c.JSON(400, models.DefaultError{
			Message: err.Error(),
		})
	}

	resp, err := h.strg.Author().GetList(models.Query{Offset: offset, Limit: limit, Search: c.Query("search")})

	if err != nil {
		c.JSON(400, models.DefaultError{
			Message: err.Error(),
		})
	}

	c.JSON(200, resp)
}

// GetByIDHandlerAuthor godoc
// @ID get-by-id-handler-author
// @Router /authors/{id} [GET]
// @Summary Get Author profile by ID
// @Description it gets an author profile by ID
// @Tags CRUD Author
// @Accept  json
// @Produce  json
// @Param id path int true "Author ID"
// @Success 200 {object} models.PersonListModel
// @Header 200 {string} Token "qwerty"
// @Failure default {object} models.DefaultError
func (h *Handler) GetByIDHandlerAuthor(c *gin.Context) {

	idStr := c.Param("id")
	idNum, err := strconv.Atoi(idStr)

	if err != nil {
		c.JSON(400, models.DefaultError{
			Message: err.Error(),
		})
		return
	}

	resp, err := h.strg.Author().GetByID(idNum)

	if err != nil {
		c.JSON(400, models.DefaultError{
			Message: err.Error(),
		})
		return
	}

	c.JSON(200, models.SuccessResponse{
		Message: "ok",
		Data:    resp,
	})
}

// UpdateHandlerAuthor godoc
// @Router /authors [PATCH]
// @Summary Update author
// @Description it updates author data from body
// @ID update-handler-author
// @Tags CRUD Author
// @Accept  json
// @Produce  json
// @Param data body models.PersonUpdateModel true "Person data"
// @Success 200  {object} models.SuccessResponse
// @Failure 400 {object} models.ErrorResponse
// @Failure default {object} models.DefaultError
func (h *Handler) UpdateHandlerAuthor(c *gin.Context) {
	var author models.PersonUpdateModel
	err := c.BindJSON(&author)
	if err != nil {
		c.JSON(400, models.ErrorResponse{
			Code:    400,
			Message: err.Error(),
		})
		return
	}

	err = h.strg.Author().Update(author)
	if err != nil {
		c.JSON(400, models.ErrorResponse{
			Code:    400,
			Message: err.Error(),
		})
		return
	}

	c.JSON(200, models.SuccessResponse{
		Message: "Ok",
	})

}

// DeleteHandlerAuthor godoc
// @Router /authors/{id} [DELETE]
// @Summary Delete Author by ID
// @Description it deletes single author by ID
// @Tags CRUD Author
// @Accept  json
// @Produce  json
// @Param id path int true "Author ID"
// @Success 200 {object} models.SuccessResponse
// @Header 200 {string} Token "qwerty"
// @Failure 400 {object} models.ErrorResponse
// @Failure default {object} models.DefaultError
func (h *Handler) DeleteHandlerAuthor(c *gin.Context) {
	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		c.JSON(400, models.ErrorResponse{
			Code:    400,
			Message: err.Error(),
		})
		return
	}

	err = h.strg.Author().Delete(id)
	if err != nil {
		c.JSON(400, models.ErrorResponse{
			Code:    400,
			Message: err.Error(),
		})
		return
	}

	c.JSON(200, models.SuccessResponse{
		Message: "deleted",
	})
}
