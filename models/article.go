package models

import "time"

type Content struct {
	Title string `json:"title" default:"Switch to Golang"`
	Body  string `json:"body" default:"Golang is an amazing language to develop softwares"`
}

type Article struct {
	ID        int        `json:"id"`
	Content              // Promoted fields
	AuthorID  int        `json:"author_id" default:""` // Nested structs
	CreatedAt *time.Time `json:"created_at" default:""`
	UpdatedAt *time.Time `json:"updated_at" default:""`
}

type ArticleListItem struct {
	ID        int        `json:"id" default:""`
	Content              // Promoted fields
	Author    Person     `json:"author"` // Nested structs
	CreatedAt *time.Time `json:"created_at" default:""`
	UpdatedAt *time.Time `json:"updated_at" default:""`
}

type ArticleCreateModel struct {
	Content      // Promoted fields
	AuthorID int `json:"author_id" default:""`
}

type ArticleUpdateModel struct {
	ID        int        `json:"ID"`
	Content              // Promoted fields
	AuthorID  int        `json:"-"`
	UpdatedAt *time.Time `json:"-"`
}
