package models

import "time"

type Person struct {
	ID        int        `json:"-"`
	Firstname string     `json:"firstname" default:"John"`
	Lastname  string     `json:"lastname" default:"Doe"`
	CreatedAt *time.Time `json:"-"`
	UpdatedAt *time.Time `json:"-"`
}

type PersonListModel struct {
	ID        int        `json:"id"`
	Firstname string     `json:"firstname"`
	Lastname  string     `json:"lastname"`
	CreatedAt *time.Time `json:"created_at"`
	UpdatedAt *time.Time `json:"updated_at"`
}

type PersonUpdateModel struct {
	ID        int    `json:"id" default:""`
	Firstname string `json:"firstname" default:"."`
	Lastname  string `json:"lastname" default:"."`
}
